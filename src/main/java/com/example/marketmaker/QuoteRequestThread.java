package com.example.marketmaker;

import java.io.*;
import java.net.Socket;

/**
 * thread class for client connection, each client connection will create a new thread
 */
public class QuoteRequestThread extends Thread {
    protected Socket socket;
    private QuoteEngineServer quoteEngineServer;

    public QuoteRequestThread(Socket clientSocket, QuoteEngineServer quoteEngineServer) {
        this.socket = clientSocket;
        this.quoteEngineServer = quoteEngineServer;
    }

    public void run() {
        InputStream inp;
        BufferedReader brinp;
        DataOutputStream out;
        try {
            inp = socket.getInputStream();
            brinp = new BufferedReader(new InputStreamReader(inp));
            out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            return;
        }
        String line;
        try {
            while ((line = brinp.readLine()) != null) {
                System.out.println("received from client:" + line);

                if (line.equalsIgnoreCase("QUIT")) {
                    socket.close();
                    return;
                } else {
                    String response = quoteEngineServer.handleQuoteRequest(line);
                    System.out.println("send response to client:" + response);
                    out.writeBytes(response + "\n");
                    out.flush();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}