package com.example.marketmaker.pricestorage;

/**
 * static data for system initialization
 */
public class SecurityStaticData {

    private int securityId;

    /**
     * previous close reference price
     */
    private double prevCloseReferencePrice;

    public SecurityStaticData(int securityId, double prevCloseReferencePrice) {
        this.securityId = securityId;
        this.prevCloseReferencePrice = prevCloseReferencePrice;
    }

    public int getSecurityId() {
        return securityId;
    }

    public double getPrevCloseReferencePrice() {
        return prevCloseReferencePrice;
    }
}
