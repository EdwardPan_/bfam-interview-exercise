package com.example.marketmaker.pricestorage;

import com.example.marketmaker.ReferencePriceSource;
import com.example.marketmaker.ReferencePriceSourceListener;
import com.example.marketmaker.marketdata.MarketDataFeed;


public class ReferencePriceSourceImpl implements ReferencePriceSource {

    private MarketDataFeed marketDataFeed;
    private ReferencePriceCache referencePriceCache;

    public ReferencePriceSourceImpl(ReferencePriceCache referencePriceCache, MarketDataFeed marketDataFeed) {
        this.marketDataFeed = marketDataFeed;
        this.referencePriceCache = referencePriceCache;
    }

    public void subscribe(ReferencePriceSourceListener listener) {
        marketDataFeed.subscribe(listener);
    }

    public double get(int securityId) {
        Double referencePrice = referencePriceCache.getReferencePrice(securityId);
        if (referencePrice == null) {
            return -1;
        } else {
            return referencePrice;
        }
    }
}
