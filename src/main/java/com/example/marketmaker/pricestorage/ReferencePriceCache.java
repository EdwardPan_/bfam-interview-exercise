package com.example.marketmaker.pricestorage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * store the securityId to reference price cache
 */
public class ReferencePriceCache {

    // can use primitive type map for better performance
    // no thread safe problem, volatile is enough as only one market data thread will update the map
    private volatile Map<Integer, Double> securityReferencePrice;

    public ReferencePriceCache(List<SecurityStaticData> staticDataList) {
        securityReferencePrice = new HashMap<>();
        for (SecurityStaticData securityStaticData : staticDataList) {
            securityReferencePrice.put(securityStaticData.getSecurityId(), securityStaticData.getPrevCloseReferencePrice());
        }
    }

    public Double getReferencePrice(int securityId) {
        return securityReferencePrice.get(securityId);
    }

    public void updateReferencePrice(int securityId, double referencePrice) {
        securityReferencePrice.put(securityId, referencePrice);
    }

}
