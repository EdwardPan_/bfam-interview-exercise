package com.example.marketmaker.pricestorage;

import java.util.ArrayList;
import java.util.List;

public class StaticDataProvider {

    public List<SecurityStaticData> getSecurityStaticData() {
        List<SecurityStaticData> staticData = new ArrayList<>();
        staticData.add(new SecurityStaticData(101, 80.0));
        staticData.add(new SecurityStaticData(102, 81.0));
        staticData.add(new SecurityStaticData(103, 82.0));
        staticData.add(new SecurityStaticData(104, 83.0));
        staticData.add(new SecurityStaticData(105, 84.0));
        return staticData;
    }
}
