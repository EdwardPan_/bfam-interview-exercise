package com.example.marketmaker.pricestorage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TargetPriceCache {

    /**
     * security id to target price map ( target price map : reference price (key) -> target price (value) )
     * can use primitive type map for better performance
     * no thread safe problem, volatile is enough as only one market data thread will update the map
     */
    private volatile Map<Integer, Map<Double, TargetPrice>> securityTargetPrice;

    public TargetPriceCache(List<SecurityStaticData> staticDataList) {

        securityTargetPrice = new HashMap<>();
        for (SecurityStaticData securityStaticData : staticDataList) {
            securityTargetPrice.put(securityStaticData.getSecurityId(), new HashMap<>());
        }
    }

    public void updateTargetPrice(int securityId, double referencePrice, double targetBuyPrice, double targetSellPrice) {
        Map<Double, TargetPrice> targetPriceMap = securityTargetPrice.get(securityId);

        if (targetPriceMap != null) {
            TargetPrice targetPrice = targetPriceMap.get(referencePrice);
            // assuming that the target price would not change given the same referencePrice
            if (targetPrice == null) {
                targetPriceMap.put(referencePrice, new TargetPrice(targetBuyPrice, targetSellPrice));
            }
        }
    }

    public Double getTargetPrice(int securityId, double referencePrice, boolean buy) {
        Map<Double, TargetPrice> targetPriceMap = securityTargetPrice.get(securityId);
        Double price = null;
        if (targetPriceMap != null) {
            TargetPrice targetPrice = targetPriceMap.get(referencePrice);

            if (targetPrice != null) {
                if (buy) {
                    price = targetPrice.getTargetBuyPrice();
                } else {
                    price = targetPrice.getTargetSellPrice();
                }
            }
        }
        return price;
    }

    public TargetPrice getTargetPriceObject(int securityId, double referencePrice) {
        Map<Double, TargetPrice> targetPriceMap = securityTargetPrice.get(securityId);
        if (targetPriceMap != null) {
            return targetPriceMap.get(referencePrice);
        } else {
            return null;
        }
    }

}
