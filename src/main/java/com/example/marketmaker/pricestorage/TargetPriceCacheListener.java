package com.example.marketmaker.pricestorage;

import com.example.marketmaker.ReferencePriceSourceListener;
import com.example.marketmaker.pricing.DerivativePricing;

/**
 * listener for updating the TargetPriceCache
 */
public class TargetPriceCacheListener implements ReferencePriceSourceListener {

    private TargetPriceCache targetPriceCache;
    private DerivativePricing derivativePricing;

    public TargetPriceCacheListener(TargetPriceCache targetPriceCache, DerivativePricing derivativePricing) {
        this.targetPriceCache = targetPriceCache;
        this.derivativePricing = derivativePricing;
    }

    public void referencePriceChanged(int securityId, double price) {

        TargetPrice targetPrice = targetPriceCache.getTargetPriceObject(securityId, price);
        if (targetPrice == null ) {
            targetPrice = derivativePricing.calculatePrice(securityId, price);
            targetPriceCache.updateTargetPrice(securityId, price, targetPrice.getTargetBuyPrice(), targetPrice.getTargetSellPrice());
        }
    }
}
