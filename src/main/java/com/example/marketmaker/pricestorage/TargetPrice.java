package com.example.marketmaker.pricestorage;

public class TargetPrice {

    private double targetBuyPrice;
    private double targetSellPrice;

    public TargetPrice(double targetBuyPrice, double targetSellPrice) {
        this.targetBuyPrice = targetBuyPrice;
        this.targetSellPrice = targetSellPrice;
    }

    public double getTargetBuyPrice() {
        return targetBuyPrice;
    }

    public double getTargetSellPrice() {
        return targetSellPrice;
    }

    public void setTargetBuyPrice(double targetBuyPrice) {
        this.targetBuyPrice = targetBuyPrice;
    }

    public void setTargetSellPrice(double targetSellPrice) {
        this.targetSellPrice = targetSellPrice;
    }
}
