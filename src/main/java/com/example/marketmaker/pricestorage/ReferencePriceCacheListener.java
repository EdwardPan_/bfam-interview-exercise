package com.example.marketmaker.pricestorage;

import com.example.marketmaker.ReferencePriceSourceListener;

/**
 * listener for updating the referencePriceCache
 */
public class ReferencePriceCacheListener implements ReferencePriceSourceListener {

    private ReferencePriceCache referencePriceCache;

    public ReferencePriceCacheListener(ReferencePriceCache referencePriceCache) {
        this.referencePriceCache = referencePriceCache;
    }

    public void referencePriceChanged(int securityId, double price) {

        referencePriceCache.updateReferencePrice(securityId, price);
    }
}
