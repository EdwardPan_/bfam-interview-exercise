package com.example.marketmaker;

import com.example.marketmaker.marketdata.MarketDataFeed;
import com.example.marketmaker.marketdata.MarketDataSimulator;
import com.example.marketmaker.pricestorage.*;
import com.example.marketmaker.pricing.DerivativePricing;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DecimalFormat;
import java.util.List;

/**
 * main class to start
 */
public class QuoteEngineServer {

    private ReferencePriceSource referencePriceSource;
    private DerivativePricing derivativePricing;
    private TargetPriceCache targetPriceCache;
    private ReferencePriceCache referencePriceCache;

    static final int PORT = 5555;

    public static void main(String... args) {
        StaticDataProvider staticDataProvider = new StaticDataProvider();
        MarketDataFeed marketDataFeed = new MarketDataSimulator(staticDataProvider.getSecurityStaticData());
        QuoteEngineServer quoteEngineServer = new QuoteEngineServer(marketDataFeed);

        ServerSocket serverSocket;
        Socket socket = null;

        try {
            serverSocket = new ServerSocket(PORT);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        while (true) {
            try {
                socket = serverSocket.accept();
            } catch (IOException e) {
                System.out.println("I/O error: " + e);
            }
            new QuoteRequestThread(socket, quoteEngineServer).start();
        }
    }

    public QuoteEngineServer(MarketDataFeed marketDataFeed) {
        derivativePricing = new DerivativePricing();
        StaticDataProvider staticDataProvider = new StaticDataProvider();
        List<SecurityStaticData> securityStaticData = staticDataProvider.getSecurityStaticData();


        targetPriceCache = new TargetPriceCache(securityStaticData);
        referencePriceCache = new ReferencePriceCache(securityStaticData);
        referencePriceSource = new ReferencePriceSourceImpl(referencePriceCache, marketDataFeed);


        referencePriceSource.subscribe(new TargetPriceCacheListener(targetPriceCache, derivativePricing));
        referencePriceSource.subscribe(new ReferencePriceCacheListener(referencePriceCache));

        Thread marketDataThread = new Thread(() -> {
            try {
                marketDataFeed.startTicking();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        marketDataThread.start();

    }


    public String handleQuoteRequest(String requestLine) {

        String[] requestParams = requestLine.split(" ");
        String errorCommand = "Bad Request";
        if (requestParams.length < 3) {
            return errorCommand;
        }
        int securityId;
        boolean buy;
        int quantity;
        try {
            securityId = Integer.parseInt(requestParams[0]);
            if ("BUY".equals(requestParams[1])) {
                buy = true;
            } else if ("SELL".equals(requestParams[1])) {
                buy = false;
            } else {
                return errorCommand;
            }
            quantity = Integer.parseInt(requestParams[2]);
        } catch (NumberFormatException e) {
            return errorCommand;
        }


        double referencePrice = referencePriceSource.get(securityId);
        if (referencePrice == -1) {
            return "Invalid SecurityId";
        }
        QuoteCalculationEngine quoteEngine = new DerivativeQuoteCalculationEngine(targetPriceCache, referencePriceCache, derivativePricing);

        DecimalFormat decimalFormat = new DecimalFormat("#.####");
        //decimalFormat.setRoundingMode(RoundingMode.FLOOR);
        return decimalFormat.format(quoteEngine.calculateQuotePrice(securityId, referencePrice, buy, quantity));
    }
}
