package com.example.marketmaker.pricing;

import com.example.marketmaker.pricestorage.TargetPrice;

/**
 * pricing library
 */
public class DerivativePricing {

    private final double PRICE_FACTOR = 0.01;
    private final double SPREAD = 0.02;

    public TargetPrice calculatePrice(int securityId, double referencePrice) {

        double targetBuyPrice = referencePrice * PRICE_FACTOR + SPREAD;
        double targetSellPrice = referencePrice * PRICE_FACTOR - SPREAD;
        return new TargetPrice(targetBuyPrice, targetSellPrice);
    }
}
