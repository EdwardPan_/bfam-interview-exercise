package com.example.marketmaker;

import java.io.*;

import java.net.Socket;

/**
 * socket client for testing
 */
public class ClientSocket {

    public static void main(String[] args) {

        try {

            Socket socket = new Socket("127.0.0.1", 5555);

            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            BufferedReader bufferedReader =new BufferedReader(new InputStreamReader(System.in,"UTF-8"));

            InputStream socketInputStream = socket.getInputStream();
            BufferedReader socketBufferReader = new BufferedReader(new InputStreamReader(socketInputStream));

            String line;
            while (true) {
                String str = bufferedReader.readLine();
                bufferedWriter.write(str);
                bufferedWriter.write("\n");
                bufferedWriter.flush();

                line = socketBufferReader.readLine();
                if ( line!= null) {
                    System.out.println("received from server:" + line);
                }
            }

        } catch (IOException e) {

            e.printStackTrace();

        }

    }

}
