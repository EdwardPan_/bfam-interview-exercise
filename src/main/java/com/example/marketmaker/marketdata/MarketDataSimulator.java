package com.example.marketmaker.marketdata;

import com.example.marketmaker.ReferencePriceSourceListener;
import com.example.marketmaker.pricestorage.SecurityStaticData;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MarketDataSimulator implements MarketDataFeed {

    private List<ReferencePriceSourceListener> referencePriceSourceListenerList = new ArrayList<>();
    private List<SecurityStaticData> staticDataList;

    public MarketDataSimulator(List<SecurityStaticData> staticDataList) {
        this.staticDataList = staticDataList;
    }

    public void startTicking() throws InterruptedException {

        // randomly generate security and reference price
        Random random = new Random();
        while (true) {
            for (SecurityStaticData securityStaticData : staticDataList) {
                // reference price will change between PrevCloseReferencePrice - 5 and PrevCloseReferencePrice + 5
                double referencePrice = securityStaticData.getPrevCloseReferencePrice() + (random.nextInt(11) - 5);
                for (ReferencePriceSourceListener listener : referencePriceSourceListenerList) {
                    listener.referencePriceChanged(securityStaticData.getSecurityId(), referencePrice);
                }
                Thread.sleep(250);
            }
        }
    }

    public void subscribe(ReferencePriceSourceListener listener) {
        referencePriceSourceListenerList.add(listener);
    }
}
