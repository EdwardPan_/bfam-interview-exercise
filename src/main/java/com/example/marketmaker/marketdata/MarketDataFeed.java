package com.example.marketmaker.marketdata;

import com.example.marketmaker.ReferencePriceSourceListener;

public interface MarketDataFeed {

    void subscribe(ReferencePriceSourceListener listener);

    void startTicking() throws InterruptedException;
}
