package com.example.marketmaker;

import com.example.marketmaker.pricestorage.ReferencePriceCache;
import com.example.marketmaker.pricestorage.TargetPrice;
import com.example.marketmaker.pricestorage.TargetPriceCache;
import com.example.marketmaker.pricing.DerivativePricing;

public class DerivativeQuoteCalculationEngine implements QuoteCalculationEngine {

    private TargetPriceCache targetPriceCache;
    private DerivativePricing derivativePricing;
    private ReferencePriceCache referencePriceCache;

    public DerivativeQuoteCalculationEngine(TargetPriceCache targetPriceCache, ReferencePriceCache referencePriceCache, DerivativePricing derivativePricing) {
        this.targetPriceCache = targetPriceCache;
        this.derivativePricing = derivativePricing;
        this.referencePriceCache = referencePriceCache;
    }

    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
        double price = -1;
        if (referencePriceCache.getReferencePrice(securityId) == null) {
            return price;
        }
        TargetPrice targetPriceObject = targetPriceCache.getTargetPriceObject(securityId, referencePrice);
        // targetPriceObject should not be null in real case, as we subscribe TargetPriceCacheListener before subscribing ReferencePriceCacheListener in QuoteEngineServer
        // when in MarketDataSimulator, we trigger the listeners in sequence
        // so as long as the given referencePrice is saved to cache, the related targetPrice price record should have been saved as well

        if ( targetPriceObject == null ) {
            TargetPrice targetPrice = derivativePricing.calculatePrice(securityId, referencePrice);
            if (buy) {
                price = targetPrice.getTargetBuyPrice();
            } else {
                price = targetPrice.getTargetSellPrice();
            }
        } else {
            if (buy) {
                price = targetPriceObject.getTargetBuyPrice();
            } else {
                price = targetPriceObject.getTargetSellPrice();
            }
        }
        if (price < 0) {
            return -1;
        } else {
            return price * quantity;
        }
    }
}
