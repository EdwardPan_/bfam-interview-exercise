package com.example.marketmaker;

import com.example.marketmaker.pricestorage.ReferencePriceCache;
import com.example.marketmaker.pricestorage.SecurityStaticData;
import com.example.marketmaker.pricestorage.TargetPriceCache;
import com.example.marketmaker.pricing.DerivativePricing;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class QuoteCalculationEngineTest {

    private QuoteCalculationEngine quoteCalculationEngine;
    private final double DELTA = 0.001;

    @BeforeAll
    public void init() {
        // should mock the DerivativePricing if it take a long time to calculate
        DerivativePricing derivativePricing = new DerivativePricing();
        List<SecurityStaticData> staticData = new ArrayList<>();

        staticData.add(new SecurityStaticData(101, 80.0));
        staticData.add(new SecurityStaticData(102, 81.0));
        staticData.add(new SecurityStaticData(103, 82.0));
        staticData.add(new SecurityStaticData(104, 83.0));
        staticData.add(new SecurityStaticData(105, 84.0));
        ReferencePriceCache referencePriceCache = new ReferencePriceCache(staticData);

        TargetPriceCache targetPriceCache = new TargetPriceCache(staticData);

        quoteCalculationEngine = new DerivativeQuoteCalculationEngine(targetPriceCache, referencePriceCache, derivativePricing);

    }

    @Test
    public void testCalculateQuotePrice() {

        assertEquals(0.82, quoteCalculationEngine.calculateQuotePrice(101, 80.0, true, 1), DELTA,
                "calculate buy price with quantity 1 and reference price 80.0");

        assertEquals(0.78, quoteCalculationEngine.calculateQuotePrice(101, 80.0, false, 1), DELTA,
                "calculate sell price with quantity 1 and reference price 80.0");

        assertEquals(8.3, quoteCalculationEngine.calculateQuotePrice(102, 81.0, true, 10), DELTA,
                "calculate buy price with quantity 10 and reference price 81.0");

        assertEquals(7.9, quoteCalculationEngine.calculateQuotePrice(103, 81.0, false, 10), DELTA,
                "calculate sell price with quantity 10 and reference price 81.0");

        // edge case - reference price 0
        assertEquals(0.02, quoteCalculationEngine.calculateQuotePrice(101, 0, true, 1), DELTA,
                "calculate buy price with quantity 1 and reference price 0");

        assertEquals(-1, quoteCalculationEngine.calculateQuotePrice(101, 0, false, 1), DELTA,
                "calculate sell price with quantity 1 and reference price 0");

        // edge case - security not in the static data list
        assertEquals(-1, quoteCalculationEngine.calculateQuotePrice(106, 0, true, 1), DELTA,
                "calculate buy price with security not exist");
    }
}
