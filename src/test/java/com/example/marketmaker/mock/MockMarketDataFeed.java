package com.example.marketmaker.mock;

import com.example.marketmaker.ReferencePriceSourceListener;
import com.example.marketmaker.marketdata.MarketDataFeed;

import java.util.ArrayList;
import java.util.List;

public class MockMarketDataFeed implements MarketDataFeed {

    private List<ReferencePriceSourceListener> referencePriceSourceListenerList = new ArrayList<>();

    public void subscribe(ReferencePriceSourceListener listener) {
        referencePriceSourceListenerList.add(listener);
    }


    public void startTicking() throws InterruptedException {

    }

    public void tick(int securityId, double referencePrice) {
        for (ReferencePriceSourceListener listener : referencePriceSourceListenerList) {
            listener.referencePriceChanged(securityId, referencePrice);
        }
    }
}
