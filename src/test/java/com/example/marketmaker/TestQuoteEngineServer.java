package com.example.marketmaker;

import com.example.marketmaker.mock.MockMarketDataFeed;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TestQuoteEngineServer {

    private QuoteEngineServer quoteEngineServer;
    private MockMarketDataFeed marketDataFeed;

    @BeforeAll
    public void init() {
        // should mock some of the service in real life scenario
        marketDataFeed = new MockMarketDataFeed();
        quoteEngineServer = new QuoteEngineServer(marketDataFeed);
    }

    @Test
    public void testHandleQuoteRequest() {
        // security 101's reference price is 80
        assertEquals("0.82", quoteEngineServer.handleQuoteRequest("101 BUY 1"),
                "reply buy price for security 101 with quantity 1");

        // security 101's reference price is 80
        assertEquals("0.78", quoteEngineServer.handleQuoteRequest("101 SELL 1"),
                "reply sell price for security 101 with quantity 1");

        // Invalid SecurityId
        assertEquals("Invalid SecurityId", quoteEngineServer.handleQuoteRequest("107 SELL 1"),
                "Test with Invalid SecurityId");

        // Bad Request
        assertEquals("Bad Request", quoteEngineServer.handleQuoteRequest("10 SELL"),
                "Bad request");
    }

    @Test
    public void testHandleQuoteRequestWithMarketDataUpdate() {

        // security 101's previous close reference price is 80
        assertEquals("0.82", quoteEngineServer.handleQuoteRequest("101 BUY 1"),
                "reply buy price for security 101 with quantity 1");

        // security 101's reference price is 80
        assertEquals("0.78", quoteEngineServer.handleQuoteRequest("101 SELL 1"),
                "reply sell price for security 101 with quantity 1");

        marketDataFeed.tick(101, 85);
        // security 101's reference price is 85
        assertEquals("0.87", quoteEngineServer.handleQuoteRequest("101 BUY 1"),
                "reply buy price for security 101 with quantity 1");

        // security 101's reference price is 85
        assertEquals("0.83", quoteEngineServer.handleQuoteRequest("101 SELL 1"),
                "reply sell price for security 101 with quantity 1");

    }
}
