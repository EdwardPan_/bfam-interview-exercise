# Interview Exercise

Implement a server that responds to quote requests.

## Requirements

The server should accept TCP connections from one or more clients and respond to their requests.  Requests are sent on a 
single line, in the format of:

    {security ID} (BUY|SELL) {quantity}

Where `security ID` and `quantity` are integers.

For example:

    123 BUY 100

Is a request to buy 100 shares of security 123.

This should be responded to with a single line with a single numeric value representing the quoted price.

To calculate the quote price, two interfaces have been provided.

* `QuoteCalculationEngine` - to calculate the quote price based on a security, buy/sell indicator, requested
quantity and reference price.
* `ReferencePriceSource` - source of reference prices for the `QuoteCalculationEngine`.

The server should be capable of handling a large number of quote requests and be able to respond in a timely manner.

## Design decision

- As QuoteCalculationEngine.calculateQuotePrice may take a long time to execute, so I decide to calcuate the price when we receive reference price update and save them in the map, which is in ReferencePriceCache and TargetPriceCache.
- If the reference price update very frequently, we can conflake them, the calculation interval depends on the frequence we got the quote request and the frequence of reference price update.
- We can pre-calcuate the price by a range of reference price at start of day, or use interpolator to avoid price calculation when we get quote request 


## How to Run

The project can run on JAVA 8, all dependency jars are under lib folder.
I use IntelliJ as IDE.
The TCP server can start by running the main method of QuoteEngineServer.
And ClientSocket is the client side for testing

## Testing

Automatic test classes are under test folder.
Manual test evidence as followed:

Client send messages:

![client side](images/client.JPG)

Server received messages and reply:

![server side](images/server.JPG)

